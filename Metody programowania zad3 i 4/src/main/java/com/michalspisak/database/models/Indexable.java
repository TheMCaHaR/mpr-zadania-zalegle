package com.michalspisak.database.models;

public interface Indexable {

    long getId();
}
